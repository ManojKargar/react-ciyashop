/**
 * Home Page One
 */
import React , {Component} from 'react';

import Slider from "react-slick";

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

class EndOfSeason extends Component {

   render() {
    return (
        <div className="row mb-7 mt-10 section-1 align-items-center">
            <div className="col-sm-12 col-lg-3 text-right mb-4">
            <div className="section-title text-sm-right">
                <h2 className="title"> End of Season</h2>
            </div>
            <div className="text-wrapper">
                <p>Avail massive discounts and get exciting offers with our shopping across all new arrival categories</p>
            </div>
            <div className="border-space" />
            <div className="ciyashop_button_wrapper ciyashop_button_width_default">
                <div className="inline_hover ciyashop_button_default ciyashop_button_border_square ciyashop_button_size_medium">
                <a className="inline_hover" href="#"> Shop Now </a>
                </div>
            </div>
            </div>
            <div className="col-sm-6 col-md-4 col-lg-3 mb-7">
            <div className="single_image-wrapper">
                <img src={require(`../assets/images/categories/default/season-img1.jpg`)} className="img-fluid attachment-full" alt="season-img1" />
            </div>
            <div className="row single_image-content">
                <div className="col-sm-3 col-2 px-0">
                <div className="border-space border-space-light" />
                </div>
                <div className="col-sm-9 col-10">
                <div className="text-wrapper">
                    <p><a href="#">Women’s</a></p>
                </div>
                </div>
            </div>
            </div>
            <div className="col-sm-6 col-md-4 col-lg-3 mb-7">
            <div className="single_image-wrapper">
                <img src={require(`../assets/images/categories/default/season-img2.jpg`)} className="img-fluid attachment-full" alt="season-img2" />
            </div>
            <div className="row single_image-content">
                <div className="col-sm-3 col-2 px-0">
                <div className="border-space border-space-dark" />
                </div>
                <div className="col-sm-9 col-10">
                <div className="text-wrapper">
                    <p><a href="#" className="text-dark">Kids</a></p>
                </div>
                </div>
            </div>
            </div>
            <div className="col-sm-12 col-md-4 col-lg-3 mb-7">
              <Slider {...settings}> 
                <div className="single_image-wrapper">
                    <img src={require(`../assets/images/categories/default/banner-video-bg.png`)} className="img-fluid attachment-full" alt="season-img2" />
                </div>
                <div className="single_image-wrapper">
                    <img src={require(`../assets/images/categories/default/season-img1.jpg`)} className="img-fluid attachment-full" alt="season-img2" />
                </div>
              </Slider>
            </div>
        </div>
  
      )
   }
}

export default EndOfSeason;