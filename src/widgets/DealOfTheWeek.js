/**
 * Home Page One
 */
import React , {Component} from 'react';
import CountDownTimer from './CountDownTimer';

class DealOfTheWeek extends Component {

   render() {
    return (
            <div className="row">
                <div className="col-sm-12 px-0">
                <div className="ciyashop_banner_wrapper" style={{backgroundImage: 'url(../assets/images/categories/default/deal-bg.png)'}}>
                <div className="ciyashop_banner ciyashop_banner-style-deal-1 ciyashop_banner-effect-none">
                    <img className="ciyashop_banner-image img-fluid inline img-fluid" alt="Banner"  src={require(`../assets/images/categories/default/deal-bg.png`)}  />
                    <div className="ciyashop_banner-content">
                        <div className="ciyashop_banner-content-wrapper">
                        <div className="ciyashop_banner-content-inner-wrapper">
                            <div className="ciyashop_banner-label-wrap ciyashop_banner-text-wrap-1 ciyashop_banner-text-bg_color hidden-lg hidden-md hidden-sm hidden-xs">
                            <div className="ciyashop_banner-label"> 50% Off</div>
                            </div>
                            <div className="ciyashop_banner-text-wrap ciyashop_banner-text-wrap-2 hidden-lg hidden-md hidden-sm hidden-xs">
                            <div className="ciyashop_banner-text">
                            <h2> Deal of The Week</h2>
                            </div>
                            </div>
                            <div className="deal-counter-wrapper mb-3 counter-size-sm counter-style-style-6  vc_custom_1540463711798">
                                <CountDownTimer time={'152350'}></CountDownTimer>
                            </div>
                            <div className="ciyashop_banner-btn-wrap ciyashop_banner-btn-style-flat ciyashop_banner-btn-size-md ciyashop_banner-btn-shape-square"> <a href="#" className="ciyashop_banner-btn inline_hover">Shop Now</a></div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
      )
   }
}

export default DealOfTheWeek;