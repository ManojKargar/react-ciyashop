/**
 * Home Page One
 */
import React , {Component} from 'react';
import { Link } from 'react-router-dom';
import Slider from "react-slick";

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

class HomSlider extends Component {

   render() {
    return (
        <Slider {...settings}>
               <div key={1} className="iron-post-item rounded iron-shadow">
                    <div className="iron-overlay-wrap">
                    <div className="iron-thumb-wrap">
                        <img src={require(`../assets/images/backgrounds/img-01.jpeg`)} alt="slide-1" />
                    </div>
                    <div className="iron-overlay-content d-flex justify-content-end align-items-center">
                        <div className="iron-overlay-holder">
                            <h2>Slider 1</h2>
                            <h1>Slider 1 Sub Title</h1>
                        </div>
                    </div>
                    </div>
                </div>  
                <div key={2} className="iron-post-item rounded iron-shadow">
                    <div className="iron-overlay-wrap">
                    <div className="iron-thumb-wrap">
                        <img src={require(`../assets/images/backgrounds/img-02.jpeg`)} alt="slide-1" />
                    </div>
                    <div className="iron-overlay-content d-flex justify-content-end align-items-center">
                        <div className="iron-overlay-holder">
                            <h2>Slider 1</h2>
                            <h1>Slider 1 Sub Title</h1>
                     </div>
                    </div>
                    </div>
                </div>  
        </Slider>
      )
   }
}

export default HomSlider;