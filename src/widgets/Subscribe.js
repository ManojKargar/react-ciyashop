/**
 * Home Page One
 */
import React , {Component} from 'react';

class Subscribe extends Component {

   render() {
    return (
        <div className="row justify-content-center banner-bg" style={{background: 'url("../assets/images/categories/default/subscribe-bg.jpg")'}}>
            <div className="col-sm-10 col-lg-6 col-md-8">
                    <div className="newsletter-wrapper newsletter-style-1 newsletter-design-4">
                    <h2 className="newsletter-title"> Subscribe today and get 25% off on your first order!</h2>
                    <div className="newsletter">
                        <div className="section-field">
                        <div className="field-widget clearfix">
                            <form className="newsletter_form">
                                <div className="input-area">
                                    <input type="text" className="placeholder newsletter-email" name="newsletter_email" placeholder="Enter your email" />
                                </div>
                                <div className="button-area">
                                    <span className="input-group-btn">
                                    <button className="btn btn-icon submit" type="submit">Subscribe</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
      )
   }
}

export default Subscribe;