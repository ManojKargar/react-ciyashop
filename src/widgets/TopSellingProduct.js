/**
 * Home Page One
 */
import React , {Component} from 'react';


class TopSellingProduct extends Component {

   render() {
    return (
        <div className="row products products-loop grid ciyashop-products-shortcode">
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-1.jpg`)}  className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img  src={require(`../assets/images/categories/default/shop-1-1.jpg`)}  className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Clothing</a>
              </span>
              <h3 className="product-name">
                 <a href="#">Women’s Fabric Mix Midi Wrap Jumpsuit</a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>9.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Luxury French fashion house Saint Laurent prides itself on constructing high-quality accessories, as showcased by this black belt, new to the brand’s AW17 collection.
                 </p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <div className="hot-sale-wrapper">
                       <span className="featured">Hot</span>
                    </div>
                    <img src={require(`../assets/images/categories/default/shop-2.jpg`)}    className="img-fluid" alt="shop" />
                    <p className="stock out-of-stock">Out of stock</p>
                 </div>
                 <div className="product-thumbnail-swap">
                    <img src={require(`../assets/images/categories/default/shop-2-1.jpg`)}    className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Bags</a>
              </span>
              <h3 className="product-name">
                 <a href="#">Women’s Accessories Vegan Leather Mini Backpack</a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>35.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Luxury French fashion house Saint Laurent prides itself on constructing high-quality accessories, as showcased by this black belt, new to the brand’s AW17 collection.
                 </p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-3.jpg`)}    className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img  src={require(`../assets/images/categories/default/shop-3-1.jpg`)}    className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Pajamas &amp; Robes</a>
              </span>
              <h3 className="product-name">
                 <a href="#">Women’s Union Suit</a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>350.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Whether you’re looking to unwind after a long day, or you simply enjoy stocking up on cute PJs, you’ll love doing so in comfort with the Union Suit from Xhilaration.</p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-4.jpg`)}   className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img  src={require(`../assets/images/categories/default/shop-4-1.jpg`)}   className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Athleisure</a>
              </span>
              <h3 className="product-name">
                 <a href="#">Women’s Crochet Shoulder Chevron Sweatshirt</a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>19.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Wearing a sweatshirt in public doesn’t mean you can’t look put-together doing it.
                 </p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-5.jpg`)}  className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img src={require(`../assets/images/categories/default/shop-5-1.jpg`)}   className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Clothing</a>
              </span>
              <h3 className="product-name">
                 <a href="#">Men’s Standard Fit Short Sleeve V-Neck T-Shirt </a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>19.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Revamp your wardrobe by going back to the basics with this Short-Sleeve V-Neck T-Shirt from Goodfellow &amp; Co.</p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-6.jpg`)}  className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img src={require(`../assets/images/categories/default/shop-6-1.jpg`)}  className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Clothing</a>
              </span>
              <h3 className="product-name">
                 <a href="#">Women’s Embellished Pointelle Open Cardigan</a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>29.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Luxury French fashion house Saint Laurent prides itself on constructing
                 high-quality accessories, as showcased by this black belt, new to the
                 brand’s AW17 collection.
                 </p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-7.jpg`)} className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img src={require(`../assets/images/categories/default/shop-7-1.jpg`)} className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Clothing</a>
              </span>
              <h3 className="product-name">
                 <a href="#"> Girls’ Long Sleeve Graphic T-Shirt – White </a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>19.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Luxury French fashion house Saint Laurent prides itself on constructing
                 high-quality accessories, as showcased by this black belt, new to the
                 brand’s AW17 collection.
                 </p>
              </div>
           </div>
           </div>
        </div>
        </div>
        <div className="col-sm-6 col-lg-3">
        <div className="product product_tag-black product-hover-style-default product-hover-button-style-dark product_title_type-single_line product_icon_type-line-icon">
           <div className="product-inner element-hovered">
           <div className="product-thumbnail">
              <div className="product-thumbnail-inner">
                 <a href="#">
                 <div className="product-thumbnail-main">
                    <img src={require(`../assets/images/categories/default/shop-8.jpg`)} className="img-fluid" alt="shop" />
                 </div>
                 <div className="product-thumbnail-swap">
                    <img src={require(`../assets/images/categories/default/shop-8-1.jpg`)} className="img-fluid" alt="shop" />
                 </div>
                 </a>
              </div>
              <div className="product-action product-action-quick-view">
                 <a href="#" className="open-quick-view" data-toggle="tooltip" data-original-title="Quick view" data-placement="right">Quick View</a>
              </div>
              <div className="product-actions">
                 <div className="product-actions-inner">
                 <div className="product-action product-action-add-to-cart">
                    <a href="#" className="button add_to_cart_button" rel="nofollow">Add to
                       cart</a>
                 </div>
                 <div className="product-action product-action-wishlist">
                    <a href="#" className="add_to_wishlist" data-toggle="tooltip" data-original-title="Wishlist" data-placement="top"> Add to
                       Wishlist</a>
                 </div>
                 <div className="product-action product-action-compare">
                    <a href="#" className="compare button" data-toggle="tooltip" data-original-title="Compare" data-placement="top">Compare</a>
                 </div>
                 </div>
              </div>
           </div>
           <div className="product-info">
              <span className="ciyashop-product-category">
                 <a href="#">Clothing</a>
              </span>
              <h3 className="product-name">
                 <a href="#"> Men’s Standard Fit Crew T-Shirt </a>
              </h3>
              <span className="price">
                 <ins>
                 <span className="price-amount amount">
                    <span className="currency-symbol">$</span>9.00
                 </span>
                 </ins>
              </span>
              <div className="product-details__short-description">
                 <p>Luxury French fashion house Saint Laurent prides itself on constructing
                 high-quality accessories, as showcased by this black belt, new to the
                 brand’s AW17 collection.
                 </p>
              </div>
           </div>
           </div>
        </div>
        </div>
                                      
         </div>
      )
   }
}

export default TopSellingProduct;