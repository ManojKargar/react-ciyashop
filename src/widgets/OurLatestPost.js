/**
 * Home Page One
 */
import React , {Component} from 'react';
import Slider from "react-slick";

const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1
  };

class OurLatestPost extends Component {

   render() {
    return (
            <div className="col-sm-12">
                <div className="latest-post-wrapper latest-post-type-carousel latest-post-style-1 latest-post-without-intro">
                    <Slider {...settings}>
                        <div className="item">
                            <div className="latest-post-item">
                                <div className="latest-post-item-inner">
                                <div className="latest-post-image">
                                    <img  src={require(`../assets/images/blog/img-01.jpg`)}  className="img-fluid" alt="blog" />
                                </div>
                                <div className="latest-post-content">
                                    <div className="post-date">
                                    <div className="post-date-inner">
                                    14<span>Aug</span>
                                    </div>
                                    </div>
                                    <div className="latest-post-meta">
                                    <ul>
                                    <li>
                                        <i className="fa fa-user" />
                                        <a href="#" rel="category tag">CiyaShop</a>
                                    </li>
                                    <li>
                                        <i className="fa fa-folder-open" />
                                        <a href="#" rel="category tag">Clothing</a>
                                    </li>
                                    </ul>
                                    </div>
                                    <h3 className="blog-title">
                                    <a href="#">Blog Post With Image</a>
                                    </h3>
                                    <div className="latest-post-excerpt">
                                    <p>I truly believe Augustine’s words are true and if you look at
                                    history you...</p>
                                    </div>
                                    <div className="latest-post-entry-footer">
                                    <a href="/">Read More...</a>
                                    <div className="latest-post-social-share">
                                    <ul>
                                        <li>
                                            <a href="#" className="facebook-share">
                                            <i className="fa fa-facebook" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="twitter-share">
                                            <i className="fa fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="linkedin-share">
                                            <i className="fa fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="googleplus-share">
                                            <i className="fa fa-google-plus" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="pinterest-share">
                                            <i className="fa fa-pinterest" />
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="latest-post-item">
                                <div className="latest-post-item-inner">
                                <div className="latest-post-image">
                                    <img src={require(`../assets/images/blog/img-02.jpg`)} className="img-fluid" alt="blog" />
                                </div>
                                <div className="latest-post-content">
                                    <div className="post-date">
                                    <div className="post-date-inner">
                                    14<span>Aug</span>
                                    </div>
                                    </div>
                                    <div className="latest-post-meta">
                                    <ul>
                                    <li>
                                        <i className="fa fa-user" />
                                        <a href="#" rel="category tag">CiyaShop</a>
                                    </li>
                                    <li>
                                        <i className="fa fa-folder-open" />
                                        <a href="#" rel="category tag">Clothing</a>
                                    </li>
                                    </ul>
                                    </div>
                                    <h3 className="blog-title">
                                    <a href="#">Blog Post With Image</a>
                                    </h3>
                                    <div className="latest-post-excerpt">
                                    <p>I truly believe Augustine’s words are true and if you look at
                                    history you...</p>
                                    </div>
                                    <div className="latest-post-entry-footer">
                                    <a href="/">Read More...</a>
                                    <div className="latest-post-social-share">
                                    <ul>
                                        <li>
                                            <a href="#" className="facebook-share">
                                            <i className="fa fa-facebook" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="twitter-share">
                                            <i className="fa fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="linkedin-share">
                                            <i className="fa fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="googleplus-share">
                                            <i className="fa fa-google-plus" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="pinterest-share">
                                            <i className="fa fa-pinterest" />
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="latest-post-item">
                                <div className="latest-post-item-inner">
                                <div className="latest-post-image">
                                    <img  src={require(`../assets/images/blog/img-03.jpg`)}  className="img-fluid" alt="blog" />
                                </div>
                                <div className="latest-post-content">
                                    <div className="post-date">
                                    <div className="post-date-inner">
                                    14<span>Aug</span>
                                    </div>
                                    </div>
                                    <div className="latest-post-meta">
                                    <ul>
                                    <li>
                                        <i className="fa fa-user" />
                                        <a href="#" rel="category tag">CiyaShop</a>
                                    </li>
                                    <li>
                                        <i className="fa fa-folder-open" />
                                        <a href="#" rel="category tag">Clothing</a>
                                    </li>
                                    </ul>
                                    </div>
                                    <h3 className="blog-title">
                                    <a href="#">Blog Post With Image</a>
                                    </h3>
                                    <div className="latest-post-excerpt">
                                    <p>I truly believe Augustine’s words are true and if you look at
                                    history you...</p>
                                    </div>
                                    <div className="latest-post-entry-footer">
                                    <a href="/">Read More...</a>
                                    <div className="latest-post-social-share">
                                    <ul>
                                        <li>
                                            <a href="#" className="facebook-share">
                                            <i className="fa fa-facebook" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="twitter-share">
                                            <i className="fa fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="linkedin-share">
                                            <i className="fa fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="googleplus-share">
                                            <i className="fa fa-google-plus" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="pinterest-share">
                                            <i className="fa fa-pinterest" />
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="latest-post-item">
                                <div className="latest-post-item-inner">
                                <div className="latest-post-image">
                                    <img  src={require(`../assets/images/blog/img-04.jpg`)}  className="img-fluid" alt="blog" />
                                </div>
                                <div className="latest-post-content">
                                    <div className="post-date">
                                    <div className="post-date-inner">
                                    14<span>Aug</span>
                                    </div>
                                    </div>
                                    <div className="latest-post-meta">
                                    <ul>
                                    <li>
                                        <i className="fa fa-user" />
                                        <a href="#" rel="category tag">CiyaShop</a>
                                    </li>
                                    <li>
                                        <i className="fa fa-folder-open" />
                                        <a href="#" rel="category tag">Clothing</a>
                                    </li>
                                    </ul>
                                    </div>
                                    <h3 className="blog-title">
                                    <a href="#">Blog Post With Image</a>
                                    </h3>
                                    <div className="latest-post-excerpt">
                                    <p>I truly believe Augustine’s words are true and if you look at
                                    history you...</p>
                                    </div>
                                    <div className="latest-post-entry-footer">
                                    <a href="/">Read More...</a>
                                    <div className="latest-post-social-share">
                                    <ul>
                                        <li>
                                            <a href="#" className="facebook-share">
                                            <i className="fa fa-facebook" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="twitter-share">
                                            <i className="fa fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="linkedin-share">
                                            <i className="fa fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="googleplus-share">
                                            <i className="fa fa-google-plus" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="pinterest-share">
                                            <i className="fa fa-pinterest" />
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                    </Slider>
                </div>
            </div>
      )
   }
}

export default OurLatestPost;

