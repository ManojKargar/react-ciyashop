import React,{Component} from 'react';
import {HashRouter,Switch,Route} from 'react-router-dom';

import Headers from './layouts/header/Header';
import Footer from './layouts/footer/Footer';

//Component
import HomePage from './component/home/index';

import './App.css';
import './Vendor.js';



class App extends Component {
  render() {
    return (
      <HashRouter>
          <Headers />
            <Switch>
              <Route exact path="/" component={HomePage} />
            </Switch>
           <Footer />
      </HashRouter>
    );
  }
}

export default App;
