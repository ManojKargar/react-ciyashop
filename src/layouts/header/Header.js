import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component {
    render() {
        return (
            <header className="site-header header-style-menu-center">
                <div className="topbar topbar-bg-color-default topbar-desktop-on topbar-mobile-off">
                <div className="container-fluid">
                    <div className="row">
                    <div className="col-lg-6 col-sm-12">
                        <div className="topbar-left text-left">
                        <div className="topbar-link">
                            <ul>
                            <li className="topbar_item topbar_item_type-currency">
                                <a href="javascript:void(0);" className=" dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-target="#my-target">USD
                                ($)</a>
                                <div className="dropdown-menu" id="my-target">
                                <a className="dropdown-item" href="#">USD ($)</a>
                                <a className="dropdown-item" href="#">EUR (€)</a>
                                </div>
                            </li>
                            <li className="topbar_item topbar_item_type-email">
                                <a href="mailto:support@ciyashop.com"><i className="fa fa-envelope-o">&nbsp;</i>support@ciyashop.com</a>
                            </li>
                            <li className="topbar_item topbar_item_type-phone_number">
                                <a href="tel:126-632-2345"><i className="fa fa-phone">&nbsp;</i>126-632-2345</a>
                            </li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                        <div className="topbar-right text-right">
                        <div className="topbar-link">
                            <ul>
                                <li className="topbar_item topbar_item_type-topbar_menu">
                                    <div className="menu-top-bar-menu-container">
                                    <ul className="top-menu list-inline">
                                        <li className="menu-item">
                                        <a href="#"><i className="fa fa-map-marker" />Stores Locator</a>
                                        </li>
                                        <li className="menu-item">
                                        <a href="#">My account</a>
                                        </li>
                                        <li>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#"><i className="fa fa-sign-in">&nbsp;</i> Login</a>
                                        </li>
                                    </ul>
                                    </div>
                                </li>
                                <li className="topbar_item topbar_item_type-social_profiles">
                                    <div className="topbar-social_profiles-wrapper">
                                    <ul className="topbar-social_profiles">
                                        <li className="topbar-social_profile">
                                        <a href="#" target="_blank"><i className="fa fa-facebook" /></a>
                                        </li>
                                        <li className="topbar-social_profile">
                                        <a href="#" target="_blank"><i className="fa fa-twitter" /></a>
                                        </li>
                                        <li className="topbar-social_profile">
                                        <a href="#" target="_blank"><i className="fa fa-google-plus" /></a>
                                        </li>
                                        <li className="topbar-social_profile">
                                        <a href="#" target="_blank"><i className="fa fa-vimeo" /></a>
                                        </li>
                                        <li className="topbar-social_profile">
                                        <a href="#" target="_blank"><i className="fa fa-pinterest" /></a>
                                        </li>
                                    </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="header-main header-main-bg-color-default">
                <div className="container-fluid">
                    <div className="row">
                    <div className="col-lg-12">
                        <div className="row align-items-center justify-content-md-center">
                        <div className="col-xl-2 col-lg-2 col-6">
                            <div className="logo-wrapper">
                            <a href="#">
                                <img className="img-fluid" src="./assets/images/logo.svg" alt="logo" />
                            </a>
                            </div>
                            <div className="clearfix" />
                        </div>
                        <div className="col" id="mainMenu">
                            <div className="header-nav header-nav-bg-color-default">
                            <div className="header-nav-wrapper">
                                <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                    <div className="primary-nav">
                                        <div className="primary-nav-wrapper">
                                        <nav id="menu" className="mega-menu" data-color>
                                            {/* menu list items container */}
                                            <div className="menu-list-items">
                                            {/* menu links */}
                                            <ul className="menu-links mega-menu-primary">
                                                {/* active class */}
                                                <li>
                                                <Link href="javascript:void(0)">Home <i className="fa fa-angle-down fa-indicator" /></Link>
                                                {/* drop down multilevel  */}
                                                <ul className="drop-down-multilevel">
                                                    <li className="mega-menu-item"><Link className="mega-menu-link" href="index.html">Home
                                                        Default</Link></li>
                                                    <li className="mega-menu-item"><a className="mega-menu-link" href="index-category.html">Home
                                                        Category</a></li>
                                                    <li className="mega-menu-item"><a className="mega-menu-link" href="index-mega.html">Home
                                                        Mega</a></li>
                                                    <li className="mega-menu-item"><a className="mega-menu-link" href="index-default-old.html">Home
                                                        Default Old</a>
                                                    </li>
                                                    <li className="mega-menu-item"><a className="mega-menu-link" href="index-new-fashion.html">Home
                                                        New Fashion</a>
                                                    </li>
                                                    <li className="mega-menu-item"><a className="mega-menu-link" href="index-classic.html">Home
                                                        Classic</a></li>
                                                    <li className="mega-menu-item"><a className="mega-menu-link" href="index-modern.html">Home
                                                        Modern</a></li>
                                                </ul>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0)">Blog <i className="fa fa-angle-down fa-indicator" /></a>
                                                {/* drop down multilevel  */}
                                                <ul className="drop-down-multilevel">
                                                    <li>
                                                    <a href="javascript:void(0)">Blog
                                                        Classic <i className="fa fa-angle-right fa-indicator" />
                                                    </a>
                                                    <ul className="drop-down-multilevel">
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-classic-left-sidebar.html">Classic
                                                            Left Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-classic-right-sidebar.html">Classic
                                                            Right Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-classic-full-width.html">Classic
                                                            Full Width</a>
                                                        </li>
                                                    </ul>
                                                    </li>
                                                    <li>
                                                    <a href="javascript:void(0)">Blog
                                                        Grid <i className="fa fa-angle-right fa-indicator" />
                                                    </a>
                                                    <ul className="drop-down-multilevel">
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-grid-left-sidebar.html">Grid
                                                            Left Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-grid-right-sidebar.html">Grid
                                                            Right Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-grid-full-width-2-columns.html">Full
                                                            Width 2 Column</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-grid-full-width-3-columns.html">Full
                                                            Width 3 Column</a>
                                                        </li>
                                                    </ul>
                                                    </li>
                                                    <li>
                                                    <a href="javascript:void(0)">Blog
                                                        Masonry <i className="fa fa-angle-right fa-indicator" />
                                                    </a>
                                                    <ul className="drop-down-multilevel">
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-masonry-left-sidebar.html">Masonry
                                                            Left Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-masonry-right-sidebar.html">Masonry
                                                            Right Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-masonry-full-width-2-columns.html">Full
                                                            Width 2 Column</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-masonry-full-width-3-columns.html">Full
                                                            Width 3 Column</a>
                                                        </li>
                                                    </ul>
                                                    </li>
                                                    <li>
                                                    <a href="javascript:void(0)">Blog
                                                        Time Line <i className="fa fa-angle-right fa-indicator" />
                                                    </a>
                                                    <ul className="drop-down-multilevel">
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-timeline-left-sidebar.html">Timeline
                                                            Left Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-timeline-right-sidebar.html">Timeline
                                                            Right Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-timeline-full-width.html">Timeline
                                                            Full Width</a>
                                                        </li>
                                                    </ul>
                                                    </li>
                                                    <li>
                                                    <a href="javascript:void(0)">Blog
                                                        Single Post <i className="fa fa-angle-right fa-indicator" />
                                                    </a>
                                                    <ul className="drop-down-multilevel">
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-single-image.html">Image
                                                            Post</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-single-youtube.html">Youtube
                                                            Video</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-single-vimeo.html">With
                                                            Vimeo Video</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-single-html-video.html">Self
                                                            Hosted Video</a>
                                                        </li>
                                                        <li className="mega-menu-item"><a className="mega-menu-link" href="blog-single-audio.html">Audio
                                                            Post</a>
                                                        </li>
                                                    </ul>
                                                    </li>
                                                </ul>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0)">Shop <i className="fa fa-angle-down fa-indicator" /></a>
                                                {/* drop down full width */}
                                                <div className="drop-down grid-col-12">
                                                    {/*grid row*/}
                                                    <div className="grid-row">
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Shop Layout</h4>
                                                        <ul>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-hidden-left-sidebar.html">Hidden
                                                            Left Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-hidden-right-sidebar.html">Hidden
                                                            Right Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-filter-style-widget.html">Filter
                                                            Style Widget</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-filter-style-toggle.html">Filter
                                                            Style Toggle</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-category-slider-style-1.html">Category
                                                            Slider Style-1</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-category-slider-style-2.html">Category
                                                            Slider Style-2</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-category-slider-style-3.html">Category
                                                            Slider Style-3</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-cart-action-default.html">Cart
                                                            Action Default</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-cart-action-off-canvas.html">Cart
                                                            Action Off
                                                            Canvas</a>
                                                        </li>
                                                        <li className="mega-menu-item mega-menu-item-13162" id="mega-menu-item-13162">
                                                            <a className="mega-menu-link" href="shop-cart-action-popup.html">Cart
                                                            Action Popup</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Product Pages</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-classic-no-sidebar.html">Classic
                                                            No Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-classic-left-sidebar.html">Classic
                                                            Left Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-classic-right-sidebar.html">Classic
                                                            Right Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-sticky-gallery-no-sidebar.html">Sticky
                                                            Gallery No
                                                            Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-sticky-gallery-left-sidebar.html">Sticky
                                                            Gallery Left
                                                            Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-sticky-gallery-right-sidebar.html">Sticky
                                                            Gallery Right
                                                            Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-wide-gallery-no-sidebar.htmlr">Wide
                                                            Gallery No
                                                            Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-wide-gallery-left-sidebar.htmlr">Wide
                                                            Gallery Left
                                                            Sidebar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-sticky-gallery-right-sidebar.html">Wide
                                                            Gallery Right
                                                            Sidebar</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Product Features</h4>
                                                        <ul>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="shop-single-thumbnail-bottom.html">Thumbnail
                                                            Bottom</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-thumbnail-left.html">Thumbnail
                                                            Left</a>
                                                        </li>
                                                        <li className="mega-hot-label mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-thumbnail-right.html">Thumbnail
                                                            Right</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-full-width.html">Page
                                                            Full Width</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-tab-layout-default.html">Tab
                                                            Layout Default</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-tab-default-center-aligned.html">Tab
                                                            Default - Center
                                                            Aligned</a>
                                                        </li>
                                                        <li className="mega-sale-label mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-tab-layout-left.html">Tab
                                                            Layout Left</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-tab-layout-accordion.html">Tab
                                                            Layout Accordion</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-single-custom-tab.html">Custom
                                                            Tab</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Shop Pages</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shopping-cart.html">My
                                                            Shopping cart</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="checkout.html">Checkout
                                                            Default</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="checkout-light-spiral.html">Checkout
                                                            Light Spiral</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="checkout-dark.html">Checkout
                                                            Dark</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="my-account.html">My
                                                            account</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="wishlist.html">Wishlist</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0)">Shop Hover <i className="fa fa-angle-down fa-indicator" /></a>
                                                {/* drop down full width */}
                                                <div className="drop-down grid-col-12">
                                                    {/*grid row*/}
                                                    <div className="grid-row">
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Product Hovers</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-default-dark.html">Default
                                                            Dark</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-default-light.html">Default
                                                            Light</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-top-left.html">Icon
                                                            Top Left</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-top-right.html">Icon
                                                            Top Right</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-center.html">Icons
                                                            Center</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Product Hovers</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-center-border.html">Icons
                                                            Center Border</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-standard-icons-left.html">Standard
                                                            Icons Left</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-standard-icons-round-dark.html">Standard
                                                            Icons Round Dark</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-button-standard.html">Button
                                                            Standard</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-left.html">Icons
                                                            Left</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Product Hovers</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-rounded.html">Icons
                                                            Rounded</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-bottom-right.html">Icons
                                                            Bottom Right</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-seperate.html">Icons
                                                            Separate</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-bottom.html">Icons
                                                            Bottom</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-bottom-round-dark.html">Icons
                                                            Bottom Round
                                                            Dark</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Product Hovers</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-bottom-bar.html">Icons
                                                            Bottom Bar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-info-bottom.html">Info
                                                            Bottom</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-info-bottom-bar.html">Info
                                                            Bottom Bar</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-hover-summary.html">Hover
                                                            Summary</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-hover-summary.html">Minimal
                                                            Hover Cart</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Product Hovers</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-minimal.html">Minimal</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-info-transparent-center.html">Info
                                                            Transparent
                                                            Center</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-icons-transparent-center.html">Icons
                                                            Transparent
                                                            Center</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-standard-info-transparent.html">Standard
                                                            Info Transparent</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="shop-standard-qucik-shop.html">Standard
                                                            Quick Shop</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0)">Pages <i className="fa fa-angle-down fa-indicator" /></a>
                                                {/* drop down full width */}
                                                <div className="drop-down grid-col-12">
                                                    {/*grid row*/}
                                                    <div className="grid-row">
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Basic Pages</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-about-us.html">About
                                                            Us</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-about-us-2.html">About
                                                            Us 2</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-faqs.html">FAQs</a>
                                                        </li>
                                                        <li className="mega-new-label mega-menu-item">
                                                            <a className="mega-menu-link" href="page-collection.html">Collection</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-information.html">Information</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-contact-us.html">Contact
                                                            Us</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-contact-us-2.html">Contact
                                                            Us 2</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Information Pages</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-terms-condition.html">Terms
                                                            &amp; Conditions</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-privacy-policy.html">Privacy
                                                            Policy</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-team-members.html">Team
                                                            Members</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-typography.html">Typography</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-payment-method.html">Payment
                                                            Method</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-shipping-method.html">Shipping
                                                            Method</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="page-warranty-and-services.html">Warranty
                                                            and Services</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Additional Pages</h4>
                                                        <ul>
                                                        <li className="mega-sale-label mega-menu-item ">
                                                            <a className="mega-menu-link" href="page-instagram-wall.html">Instagram
                                                            Wall</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="page-instagram-shop.html">Instagram
                                                            Shop</a>
                                                        </li>
                                                        <li className="mega-menu-item" id="mega-menu-item-9826">
                                                            <a className="mega-menu-link" href="page-look-book.html">Look
                                                            Book</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="page-modern-process.html">Modern
                                                            Process</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="page-parallax-presentation.html">Parallax
                                                            Presentation</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="page-stores-locator.html">Stores
                                                            Locator</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="page-store-list.html">Store
                                                            List</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Portfolio</h4>
                                                        <ul>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-style-1.html">Portfolio
                                                            Style 01</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-style-2.html">Portfolio
                                                            Style 02</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-style-3.html">Portfolio
                                                            Style 03</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-style-4.html">Portfolio
                                                            Style 04</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-style-5.html">Portfolio
                                                            Style 05</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-slider.html">Portfolio
                                                            Slider</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-sticky.html">Portfolio
                                                            Sticky</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="portfolio-gallery.html">Portfolio
                                                            Gallery</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-2">
                                                        <h4>Extra Pages</h4>
                                                        <ul>
                                                        <li className="mega-popular-label mega-menu-item ">
                                                            <a className="mega-menu-link" href="error-404.html">404</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="coming-soon.html">Coming
                                                            Soon</a>
                                                        </li>
                                                        <li className="mega-menu-item ">
                                                            <a className="mega-menu-link" href="maintenance.html">Maintenance</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    <div className="grid-col-6">
                                                        <a href="#"><img src="assets/images/menu-banner1.jpg" alt="Big Sale 50% OFF" className="img-fluid" width={600} height={220} /></a>
                                                    </div>
                                                    <div className="grid-col-6">
                                                        <a href="#"><img src="assets/images/menu-banner2.jpg" alt="Fashion Sale 50% OFF" className="img-fluid" width={600} height={220} /></a>
                                                    </div>
                                                    </div>
                                                </div>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0)">Elements <i className="fa fa-angle-down fa-indicator" /></a>
                                                {/* drop down full width */}
                                                <div className="drop-down grid-col-12">
                                                    {/*grid row*/}
                                                    <div className="grid-row">
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Ciya elements 01</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-address-block.html">Address
                                                            block</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-banners.html">Banner</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-carousels.html">Carousels</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-category-box.html">Category
                                                            box</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Ciya elements 02</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-clients-logo.html">Clients
                                                            logo</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-instagram.html">Instagram</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-images-slider.html">Images
                                                            slider</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-list.html">List</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Ciya elements 03</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-news-letter.html">Newsletter</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-product-deals.html">Product
                                                            deals</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-product-listing.html">Product
                                                            listing</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    {/*grid column 2*/}
                                                    <div className="grid-col-3">
                                                        <h4>Ciya elements 04</h4>
                                                        <ul>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-recent-posts.html">Recent
                                                            posts</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-social-icon.html">Social
                                                            icon</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-team-member.html">Team
                                                            member</a>
                                                        </li>
                                                        <li className="mega-menu-item">
                                                            <a className="mega-menu-link" href="elements-testimonials.html">Testimonials</a>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                                </li>
                                            </ul>
                                            </div>
                                        </nav>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-2 col-6">
                            <div className="header-nav-right-wrapper">
                            <div className="ciya-tools">
                                <div className="ciya-tools-wrapper">
                                <ul className="ciya-tools-actions">
                                    <li className="ciya-tools-action ciya-tools-cart">
                                    <a className="cart-link" href="#" title="View Cart (0)">
                                        <span className="cart-icon"><i className="glyph-icon pgsicon-ecommerce-empty-shopping-cart" /></span>
                                        <span className="cart-count count">0</span>
                                    </a>
                                    <div className="cart-contents">
                                        <div className="widget ciyashop widget-shopping-cart">
                                        <div className="widget-shopping-cart-content">
                                            <div className="pgs-product-list-widget-container has-scrollbar">
                                            <ul className="ciyashop-mini-cart cart-list">
                                                <li className="ciya-mini-cart-item">
                                                <a href="#" className="remove remove_from_cart_button">×</a>
                                                <div className="media">
                                                    <a href="#"><img width={60} height={76} src="assets/images/shop/img-01.jpg" className="img-fluid" alt /></a>
                                                    <div className="media-body">
                                                    <a href="#" className="product-title">Buckle belt – Saint Laurent</a>
                                                    <span className="quantity">1 × <span className="woocs-special_price_code"><span className="ciya-Price-amount amount"><span className="ciya-Price-currencySymbol">$</span>40.00</span></span></span>
                                                    </div>
                                                </div>
                                                </li>
                                                <li className="ciya-mini-cart-item">
                                                <a href="#" className="remove remove_from_cart_button">×</a>
                                                <div className="media">
                                                    <a href="#"><img width={60} height={76} src="assets/images/shop/img-02.jpg" className="img-fluid" alt /></a>
                                                    <div className="media-body">
                                                    <a href="#" className="product-title">Extra Fine Wool Jumpers </a>
                                                    <span className="quantity">1 × <span className="woocs-special_price_code"><span className="ciya-Price-amount amount"><span className="ciya-Price-currencySymbol">$</span>40.00</span></span></span>
                                                    </div>
                                                </div>
                                                </li>
                                                <li className="ciya-mini-cart-item">
                                                <a href="#" className="remove remove_from_cart_button">×</a>
                                                <div className="media">
                                                    <a href="#"><img width={60} height={76} src="assets/images/shop/img-03.jpg" className="img-fluid" alt /></a>
                                                    <div className="media-body">
                                                    <a href="#" className="product-title">Girl's Cold Shoulder Bling Dress – Black</a>
                                                    <span className="quantity">1 × <span className="woocs-special_price_code"><span className="ciya-Price-amount amount"><span className="ciya-Price-currencySymbol">$</span>116.00</span></span></span>
                                                    </div>
                                                </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <p className="ciyashop-mini-cart__total total"><strong>Subtotal:</strong> <span className="woocs_special_price_code"><span className="ciyashop-Price-amount amount"><span className="ciyashop-Price-currencySymbol">$</span>196.00</span></span></p>
                                            <p className="ciyashop-mini-cart__buttons buttons">
                                            <a href="shopping-cart.html" className="button wc-forward">View cart</a>
                                            <a href="checkout.html" className="button checkout wc-forward">Checkout</a>
                                            </p>
                                        </div>
                                        </div>
                                    </div>
                                    </li>
                                    <li className="ciya-tools-action ciya-tools-compare"> <a href="#compare" className="compare" rel="nofollow"> <i className="glyph-icon pgsicon-ecommerce-arrows-7" /> </a></li>
                                    <li className="ciya-tools-action ciya-tools-wishlist"> <a href="wishlist.html"><i className="glyph-icon pgsicon-ecommerce-like" /> <span className="wishlist ciyashop-wishlist-count"> 0 </span> </a></li>
                                </ul>
                                </div>
                            </div>
                            <div className="header-search-wrap">
                                <div className="search-button-wrap">
                                <button type="button" className="btn btn-primary btn-lg search-button" data-toggle="modal" data-target="#search-popup">
                                    <i className="glyph-icon pgsicon-ecommerce-magnifying-glass" />
                                </button>
                                <span>Search</span>
                                </div>
                            </div>
                            <div className="mobile-butoon mobile-butoon-menu">
                                <a className="mobile-menu-trigger mobile-menu-trigger-closed" href="javascript:void(0)">
                                <span />
                                </a>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="row">
                    <div className="col-12">
                        <div className="mobile-menu" id="mobileMenu" />
                    </div>
                    </div>
                </div>
                </div>
                <div className="mobile-nav">
                <ul className="mobile-nav-menu slicknav" id="mobileNav">
                    <li>
                    <a href="javascript:void(0)">Home <i className="fa fa-angle-down fa-indicator" /></a>
                    {/* drop down multilevel  */}
                    <ul className="drop-down-multilevel">
                        <li><a href="index.html">Home
                            Default</a></li>
                        <li><a href="index-category.html">Home
                            Category</a></li>
                        <li><a href="index-mega.html">Home
                            Mega</a></li>
                        <li><a href="index-default-old.html">Home
                            Default Old</a>
                        </li>
                        <li><a href="index-new-fashion.html">Home
                            New Fashion</a>
                        </li>
                        <li><a href="index-classic.html">Home
                            Classic</a></li>
                        <li><a href="index-modern.html">Home
                            Modern</a></li>
                    </ul>
                    </li>
                </ul>
                </div>
            </header>
        )
    }
};
export default Header;