import React from 'react';

class Footer extends React.Component {
    render() {
        return (
                <footer className="site-footer">
                <div className="footer-wrapper">
                    <div className="footer-widgets-wrapper">
                    <div className="footer">
                        <div className="container">
                        <div className="row">
                            <div className="col-lg-3 col-md-6 footer-align-left">
                            <div className="logo-wrapper widget">
                                <p><a href="#">
                                    <img className="img-fluid"  src={require(`../../assets/images/logo.svg`)}   alt="logo" />
                                </a></p>
                            </div>
                            <div className="text-content">
                                <p>CiyaShop is an enchanting and easy to use e-Commerce WP theme that allows you to sell your products in a dynamic way.</p>
                                <p>The theme is packed with everything you need to create a new website.</p>
                            </div>
                            <div className="pgs-social-profiles">
                                <div className="social-profiles-wrapper">
                                <div className="social-profiles-wrapper-inner social-profiles-default social-profiles-shape-square">
                                    <div className="social-profiles">
                                    <ul>
                                        <li><a href="#" title="Facebook" target="_blank"><i className="fa fa-facebook" /></a></li>
                                        <li><a href="#" title="Twitter" target="_blank"><i className="fa fa-twitter" /></a></li>
                                        <li><a href="#" title="Googleplus" target="_blank"><i className="fa fa-google-plus" /></a></li>
                                        <li><a href="#" title="Vimeo" target="_blank"><i className="fa fa-vimeo" /></a></li>
                                        <li><a href="#" title="Pinterest" target="_blank"><i className="fa fa-pinterest" /></a></li>
                                    </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-align-left">
                            <div className="footer-nav-menu widget">
                                <h4 className="footer-title title">Useful Links</h4>
                                <div className="menu-useful-links-container">
                                <ul className="menu">
                                    <li className="menu-item active"><a href="#">Home</a></li>
                                    <li className="menu-item"><a href="#">About Us</a></li>
                                    <li className="menu-item"><a href="#">Stores Locator</a></li>
                                    <li className="menu-item"><a href="#">Shop</a></li>
                                    <li className="menu-item"><a href="#">Contact Us</a></li>
                                    <li className="menu-item"><a href="#">Privacy Policy</a></li>
                                    <li className="menu-item"><a href="#">Terms Conditions</a></li>
                                </ul>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-align-left">
                            <div className="footer-nav-menu widget">
                                <h4 className="footer-title title">Information</h4>
                                <div className="menu-useful-links-container">
                                <ul className="menu">
                                    <li className="menu-item active"><a href="#">Look Book</a></li>
                                    <li className="menu-item"><a href="#">Information</a></li>
                                    <li className="menu-item"><a href="#">Instagram Wall</a></li>
                                    <li className="menu-item"><a href="#">Typography</a></li>
                                    <li className="menu-item"><a href="#">Parallax Presentation</a></li>
                                    <li className="menu-item"><a href="#">Modern Process</a></li>
                                    <li className="menu-item"><a href="#">Warranty and Services</a></li>
                                </ul>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-align-left">
                            <div className="pgs-contact-widget widget">
                                <h4 className="footer-title title">Information</h4>
                                <div className="footer-address">
                                <ul>
                                    <li><i className="fa fa-map-marker" /><span>1635 Franklin Street Montgomery, Near Sherwood Mall. AL 36104</span></li>
                                    <li className="pgs-contact-email"><i className="fa fa-envelope-o" /><span>support@ciyashop.com</span></li>
                                    <li><i className="fa fa-phone" /><span>126-632-2345</span></li>
                                </ul>
                                </div>
                            </div>
                            <div className="widget pgs-newsletter-widget">
                                <h4 className="footer-title title">Newsletter</h4>
                                <div className="newsletter">
                                <div className="section-field">
                                    <form className="newsletter_form">
                                    <div className="input-area">
                                        <input type="text" className="placeholder newsletter-email" name="newsletter_email" placeholder="Enter your email" />
                                    </div>
                                    <div className="button-area">
                                        <span className="input-group-btn">
                                        <button className="btn btn-icon newsletter-mailchimp submit" type="submit">Subscribe</button>
                                        </span>
                                        <span className="newsletter-spinner spinimg-pgs_newsletter_widget_2" />
                                    </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="footer-bottom-wrapper">
                    <div className="container">
                        <div className="row">
                        <div className="col-12">
                            <div className="footer-bottom">
                            <div className="row align-items-center">
                                <div className="col-lg-6">
                                <div className="row">
                                    <div className="col-12">
                                    <div className="footer-content">
                                        CiyaShop Mobile app is Available now. Download it now on your favorite device and indulge in a seamless shopping experience.
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <div className="col-lg-6">
                                <div className="app-group row text-lg-right">
                                    <div className="col-md-4">
                                    <div className="app-img">
                                        <img src={require(`../../assets/images/appbtntext.png`)} className="img-fluid" alt />
                                    </div>
                                    </div>
                                    <div className="col-md-8">
                                    <a href="#" className="apps-store-img">
                                        <img src={require(`../../assets/images/google-play-img.png`)} className="img-fluid" alt />
                                    </a>
                                    <a href="#" className="apps-store-img">
                                        <img src={require(`../../assets/images/appstorebtn.png`)} className="img-fluid" alt />
                                    </a>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="site-info">
                    <div className="footer-widget">
                        <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-6 float-left">
                            <p> © Copyright 2019 <a href="#">CiyaShop</a> All Rights Reserved.</p>
                            </div>
                            <div className="col-md-6 float-right">
                            <div className="payments text-right">
                                <img src={require(`../../assets/images/payments.png`)} className="img-fluid" alt />
                            </div>
                            </div>
                        </div>
                        <div className="clearfix" />
                        </div>
                    </div>
                    </div>
                  </div>
                </footer>
        )
    }
};
export default Footer;