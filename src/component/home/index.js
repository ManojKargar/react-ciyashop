/**
 * Home Page One
 */
import React , {Component} from 'react';
import HomeSlider from '../../widgets/HomSlider.js';
import EndOfSeason from '../../widgets/EndOfSeason.js';
import TopSellingProduct from '../../widgets/TopSellingProduct.js';
import HomeBanner from '../../widgets/HomeBanner.js';
import DealOfTheWeek from '../../widgets/DealOfTheWeek.js';
import AboutBanner from '../../widgets/AboutBanner.js';
import Subscribe from '../../widgets/Subscribe.js';
import OurLatestPost from '../../widgets/OurLatestPost.js';

class HomePage extends Component {

   render() {
    return (
         <div>
            <HomeSlider />
            <div id="content" className="site-content" tabIndex={-1}>
            <div className="content-wrapper content-wrapper-vc-enabled">
               <div id="primary" className="content-area">
                  <main id="main" className="site-main">
                     <article id="post-13069" className="post-13069 page type-page status-publish hentry">
                        <div className="entry-content">
                           <div className="container">
                                 <EndOfSeason />
                           </div>
                           <div className="container">
                                    <div className="row mb-md-7">
                                       <div className="col-sm-12">
                                          <div className="row mb-3">
                                          <div className="col-sm-10 col-lg-8 offset-lg-2 offset-sm-1 text-center">
                                             <div className="section-title">
                                                <h2 className="title"> Top Selling Products</h2>
                                             </div>
                                             <div className="row">
                                                <div className="col-sm-10 col-lg-8 offset-lg-2 offset-sm-1">
                                                <div className="text-wrapper">
                                                   <p>Forget about struggling to do everything at once: taking care of the family, running your business etc.</p>
                                                </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>
                                          <TopSellingProduct />
                                       </div>
                                    </div>
                              </div>
                              <div className="container">
                                 <HomeBanner />
                              </div>
                              <div className="container-fluid section-2">
                                 <DealOfTheWeek />
                              </div>
                              <div className="container section-3">
                                 <AboutBanner /> 
                              </div>
                              <div className="container">
                                 <Subscribe />
                              </div>
                              <div className="container">
                                    <div className="row mt-md-10 mt-5 mb-md-10 mb-5">
                                       <div className="col-sm-12">
                                          <div className="row text-center">
                                             <div className="col-sm-12 offset-lg-2 col-lg-8 offset-md-1 col-md-9">
                                                <div className="section-title">
                                                   <h2 className="title"> Our Latest Post</h2>
                                                </div>
                                                <div className="text-wrapper mb-4">
                                                   <p>Read what we say on our blog. you do not have to worry about getting stuck alone trying to figure everything out.</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div className="row">
                                             <OurLatestPost />
                                           </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                       </article>
                     </main>
                  </div>
               </div>
            </div>
         </div>
      )
   }
}

export default HomePage;